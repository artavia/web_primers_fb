(function() {
  
  const config = {
    apiKey: ""
    , authDomain: ""
    , databaseURL: ""
    , projectId: ""
    , storageBucket: ""
    , messagingSenderId: ""
  };
  
  var defaultApp = firebase.initializeApp(config); // console.log( "defaultApp: " ,  defaultApp );

  // get the elements
  const txtEmail = document.querySelector('#txtEmail');
  const txtPassword = document.querySelector('#txtPassword');
  const btnLogin = document.querySelector('#btnLogin');
  const btnSignUp = document.querySelector('#btnSignUp');
  const btnLogout = document.querySelector('#btnLogout');
  const statusLoggedIn = document.querySelector( '#statusLoggedIn' );
  const defaultAuth = defaultApp.auth();
  
  btnLogout.addEventListener( "click" , UserEmPwLogout );
  function UserEmPwLogout( e ) {  // Add logout event
    defaultAuth.signOut();
  }
  
  btnLogin.addEventListener( "click" , EmPwUserLogin );
  function EmPwUserLogin(e) {  // Add login event 
    let email = txtEmail.value;
    let pass = txtPassword.value;
    let promise = defaultAuth.signInWithEmailAndPassword( email, pass );
    promise.then( verifyAuth ).catch( DerppUserSignIn );
  }

  function DerppUserSignIn(e) {
    console.log( "e.message" , e.message ); console.log( "e.code" , e.code );
  }

  function verifyAuth(){  // Add listener to be notified each time that auth state changes...
    return defaultAuth.onAuthStateChanged( LoggedInOrOut ); 
  }

  function LoggedInOrOut(firebaseUser) {
    if( firebaseUser ){ 
      console.log( "firebaseUser " , firebaseUser );

      btnLogout.classList.remove( 'hide' );
      statusLoggedIn.classList.remove( 'hide' );

      var displayName = firebaseUser.displayName; // null
      var email = firebaseUser.email;
      var emailVerified = firebaseUser.emailVerified; // false
      var photoURL = firebaseUser.photoURL; // null
      var isAnonymous = firebaseUser.isAnonymous; // false
      var uid = firebaseUser.uid;
      var providerData = firebaseUser.providerData; // array
    } 
    else 
    if ( firebaseUser === null ) { 
      btnLogout.classList.add( 'hide' );
      statusLoggedIn.classList.add( 'hide' );
    }
  }

  btnSignUp.addEventListener( "click" , CreateEmPw );
  function CreateEmPw(e) {  // Add signup event AND  // TODO: insert email validation code
    let email = txtEmail.value;
    let pass = txtPassword.value;
    let promise = defaultAuth.createUserWithEmailAndPassword( email, pass );
    promise.then( verifyAuth ).catch( DerppCreateUser );
  }

  function DerppCreateUser( e ) { 
    console.log( "e.message" , e.message ); console.log( "e.code" , e.code ); 
  }

})();