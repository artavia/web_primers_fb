(function() {
  
  const config = {
    apiKey: ""
    , authDomain: ""
    , databaseURL: ""
    , projectId: ""
    , storageBucket: ""
    , messagingSenderId: ""
  };
  
  var defaultApp = firebase.initializeApp(config);

  // get the elements
  const btnLogin = document.querySelector('#btnLogin');
  const btnLogout = document.querySelector('#btnLogout');
  const statusLoggedIn = document.querySelector( '#statusLoggedIn' );
  const defaultAuth = firebase.auth();
  
  btnLogin.addEventListener( "click" , e => {  // Add login event 
    let promise = defaultAuth.signInAnonymously();
    promise.then( verifyAuth ).catch( (e) => { console.log( e.message ) } );
  } );

  btnLogout.addEventListener( "click" , e => {  // Add logout event
    defaultAuth.signOut();
  } );

  function verifyAuth(){ 
    return defaultAuth.onAuthStateChanged( firebaseUser => {
      if( firebaseUser ){ 
        console.log( "firebaseUser " , firebaseUser );

        btnLogout.classList.remove( 'hide' );
        statusLoggedIn.classList.remove( 'hide' );
      } 
      else 
      if ( firebaseUser === null ) { 
        btnLogout.classList.add( 'hide' );
        statusLoggedIn.classList.add( 'hide' );
      }
    } ); 
  } 

})();