(function() {
  
  const config = {
    apiKey: ""
    , authDomain: ""
    , databaseURL: ""
    , projectId: ""
    , storageBucket: ""
    , messagingSenderId: ""
  };
  
  firebase.initializeApp(config);

  // get the elements
  const txtEmail = document.querySelector('#txtEmail');
  const txtPassword = document.querySelector('#txtPassword');
  const btnLogin = document.querySelector('#btnLogin');
  const btnSignUp = document.querySelector('#btnSignUp');
  const btnLogout = document.querySelector('#btnLogout');
  const statusLoggedIn = document.querySelector( '#statusLoggedIn' );
  const auth = firebase.auth();

  const hotelOne = document.querySelector('#hotelOne'); 
  const signedIn = document.querySelector('#signedIn');
  const signedOut = document.querySelector('#signedOut');
  
  const dbRef = firebase.database().ref();
  
  const teststring = dbRef.child( 'teststring' );
  const testobject = dbRef.child( 'testobject' );
  const grocerylist = testobject.child( 'grocerylist' );

  const preObject = document.querySelector('#outputObj');
  const ulList = document.querySelector('#outputList');

  btnLogin.addEventListener( "click" , e => {  // Add login event 
    let email = txtEmail.value;
    let pass = txtPassword.value;
    let promise = auth.signInWithEmailAndPassword( email, pass );
    promise.then( verifyAuth ).catch( (e) => { console.log( e.message ) } );
  } );
  
  btnSignUp.addEventListener( "click" , e => {  // Add signup event AND  // TODO: insert email validation code
    let email = txtEmail.value;
    let pass = txtPassword.value;
    let promise = auth.createUserWithEmailAndPassword( email, pass );
    promise.then( verifyAuth ).catch( (e) => { console.log( e.message ) } );
  } );

  btnLogout.addEventListener( "click" , e => {  // Add logout event
    auth.signOut();
  } );

  function verifyAuth(){  // Add a realtime listener to notify me of each time that fbusers auth state changes...
    return auth.onAuthStateChanged( firebaseUser => {
      // console.log( "firebaseUser " , firebaseUser );      
      if( firebaseUser ){ 
        
        teststring.on( 'value' , (snap) => { hotelOne.innerText = snap.val(); } );
        testobject.on( 'value' , (snap) => { // console.log( snap.val() ); 
          var value = snap.val(), replacer = null, space = 3;
          preObject.innerText = JSON.stringify( value, replacer, space );
        } );

        grocerylist.on( 'child_changed', (snap) => { // console.log( snap.key );
          let liChanged = document.getElementById( snap.key );
          liChanged.innerText = snap.val();
        } );

        grocerylist.on( 'child_removed', (snap) => { // console.log( snap.key );
          let liToRemove = document.getElementById( snap.key );
          liToRemove.remove();
        } );

        grocerylist.on( 'child_added', (snap) => { // console.log( typeof( snap.val() ) ); 
          if( typeof( snap.val() ) === "string" || typeof( snap.val() ) === "number" ){
            let li = document.createElement('li');
            li.innerText = `${snap.key}: ${snap.val()}`;
            li.id = snap.key;
            ulList.appendChild(li);
          }
          else
          if( typeof( snap.val() ) === "object" ){
            if( Array.isArray( snap.val() ) === true ){ // console.log( "array: ", snap.val() );
              // Array Object with keys and values
              let li = document.createElement('li');
              var arrLen = snap.val().length;
              let embeddedUl = document.createElement('ul');
              var a;
              for( a = 0; a < arrLen; a = a + 1 ){
                let embeddedli = document.createElement('li');
                embeddedli.innerText = snap.val()[a];
                embeddedli.id = `${snap.key}`;
                embeddedUl.appendChild(embeddedli);
              }
              li.innerText = `${snap.key}: `;
              li.appendChild(embeddedUl);
              li.id = snap.key;
              ulList.appendChild(li);
            }
            else
            if( Array.isArray( snap.val() ) === false ){ // console.log( "object: ", snap.val() );
              // Object literal with props
              let li = document.createElement('li');
              let embeddedUl = document.createElement('ul');
              var objLitrl = snap.val();
              var hk;
              for( hk in objLitrl ){
                let embeddedli = document.createElement('li');
                embeddedli.innerText = `${hk}: ${ objLitrl[hk] }`;
                embeddedli.id = `${hk}`;
                embeddedUl.appendChild(embeddedli);
              }
              li.innerText = `${snap.key}: `;
              li.appendChild(embeddedUl);
              li.id = snap.key;
              ulList.appendChild(li);
            }
          }

        } );

        btnLogout.classList.remove( 'hide' );
        statusLoggedIn.classList.remove( 'hide' );

        signedOut.classList.add( 'hide' );
        signedIn.classList.remove( 'hide' );
        hotelOne.classList.remove( 'hide' );
        preObject.classList.remove( 'hide' );
        ulList.classList.remove( 'hide' );
      } 
      else 
      if ( firebaseUser === null ) { 
        btnLogout.classList.add( 'hide' );
        statusLoggedIn.classList.add( 'hide' );

        signedOut.classList.remove( 'hide' );
        signedIn.classList.add( 'hide' );
        hotelOne.classList.add( 'hide' );
        preObject.classList.add( 'hide' );
        ulList.classList.add('hide');
        hotelOne.innerText = "";
        preObject.innerText = "";
        ulList.innerHTML = "";
      }
    } ); 
  }  

})();