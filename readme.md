# Description
Firebase for web primer&hellip; yeah, it&rsquo;s a thing!

## What it is
It&rsquo;s an appetizer. It&rsquo;s like primer that goes on an automobile before a new paint job. The purpose of this is to introduce you to the subject of [Firebase](https://firebase.google.com/ "link to Firebase"). Firebase is a Google Webservice that can optionally be added to new or existing projects. It all depends on your needs. For the purposes of aestheticism, I have also baked-in some basic [Bootstrap](http://getbootstrap.com/ "link to Bootstrap") functionality.

## Preparatory steps
You will need a **text editor** and a deploy&#45;anywhere, front&#45;end oriented webserver such as **[http-server](https://www.npmjs.com/package/http-server "link to npm repository for http-server")** (which I recommend that you install globally) or **Web Server for Chrome**.

## What you REALLY need
You need to obtain at least one Google Api Key and Configuration Object as it corresponds to the Firebase Web Service. I recommend that you get **two** for the purposes of following along. Without an API key, you will not get far at all. When you do obtain a key, you can paste in the Configuration Object in a document called  **app.js** in each of the respective folders. Also, visiting the page called [Add Firebase to your JavaScript Project](https://firebase.google.com/docs/web/setup "link to Add Firebase to your JavaScript Project") should bring you up to speed. 

## How to use it
Basically, I watched about a half a dozen videos on the subject to get acquainted. The videos were done by [Firebase](https://www.youtube.com/channel/UCP4bf6IHJJQehibu6ai__cg "link to Firebase channel"). Five of the videos have to do with **auth**, per se, while the other remaining video deals with **file storage**. In essence, these are the videos by Google that I watched and here is how they correspond to the file structure contained here. All of these are mutually exclusive from one to the next. But I decided to **always** include some level of authorization in my project despite the instructions in the video. It breaks down like this:
  - The folder found at **a-get_started/001-get_started/** corresponds with the video called [Getting Started with Firebase on the Web](https://www.youtube.com/watch?v=k1D0_wFlXgo "link to Getting Started with Firebase on the Web");
  - The folder found at **c-auth/004-get_started** corresponds with the video called [Getting started with Firebase Auth on the Web](https://www.youtube.com/watch?v=-OKrloDzGpU "link to Getting started with Firebase Auth on the Web");
  - The folder found at **c-auth/012-anon_authentication** corresponds with the video called [Getting Started with Firebase Anonymous Authentication on the Web](https://www.youtube.com/watch?v=ApG8L2RKrSI "link to Getting Started with Firebase Anonymous Authentication on the Web");
  - The folder found at **d-realtime-database/005-get_started** corresponds with the video called [Getting Started with the Firebase Realtime Database on the Web, Part 1](https://www.youtube.com/watch?v=noB98K6A0TY "link to Getting Started with the Firebase Realtime Database on the Web, Part 1");
  - The folder found at **d-realtime-database/007-part_ii** corresponds with the video called [Getting Started with the Firebase Realtime Database on the Web, Part 2](https://www.youtube.com/watch?v=dBscwaqNPuk "link to Getting Started with the Firebase Realtime Database on the Web, Part 2").

All of the preceeding tutorials can easily correspond with the same Firebase database. This constitutes the first of two Google Api Keys and Configuration Objects. The final lesson, which deals with storage, should have a different setup altogether. Therefore:
  - The folder found at **b-storage/002-get_started** corresponds with the video called [Getting Started with Firebase Storage on the Web](https://www.youtube.com/watch?v=SpxHVrpfGgU "link to Getting Started with Firebase Storage on the Web").

## A note on the structure of the testing object
For the videos dealing with **auth**, you are going to eventually deal with building a POJO (plain ol&rsquo; javascript object) based database. Here is how mine was broken down which should help you acclimate to your surroundings (at least in the context of this web project): 
```sh
{
   "bff": "Jimmy Janks",
   "grocerylist": {
      "cereals": {
         "black-sheep": "golden fringed rainbows",
         "kids": "flex flakes",
         "parents": "mooselicks"
      },
      "coffee": "Maxi-café",
      "produce": [
         "strawberries",
         "endive",
         "romaine",
         "chicory"
      ],
      "recyclablebags": 5
   },
   "hobbies": "eating, sleeping, being lazy",
   "name": "Billy Badass"
}
```

## My name is Luis
Or call and ask for don Lucho if you want to invite me for a job interview.