(function() {
  
  const config = {
    apiKey: ""
    , authDomain: ""
    , databaseURL: ""
    , projectId: ""
    , storageBucket: ""
    , messagingSenderId: ""
  };
  
  firebase.initializeApp(config);

  // get the elements
  const txtEmail = document.querySelector('#txtEmail');
  const txtPassword = document.querySelector('#txtPassword');
  const btnLogin = document.querySelector('#btnLogin');
  const btnSignUp = document.querySelector('#btnSignUp');
  const btnLogout = document.querySelector('#btnLogout');
  const statusLoggedIn = document.querySelector( '#statusLoggedIn' );
  const auth = firebase.auth();

  const hotelOne = document.querySelector('#hotelOne'); 
  const signedIn = document.querySelector('#signedIn');
  const signedOut = document.querySelector('#signedOut');
  
  const dbRef = firebase.database().ref();
  
  const teststring = dbRef.child( 'teststring' );
  

  const testobject = dbRef.child( 'testobject' );

  btnLogin.addEventListener( "click" , e => {  // Add login event 
    let email = txtEmail.value;
    let pass = txtPassword.value;
    let promise = auth.signInWithEmailAndPassword( email, pass );
    promise.then( verifyAuth ).catch( (e) => { console.log( e.message ) } );
  } );
  
  btnSignUp.addEventListener( "click" , e => {  // Add signup event AND  // TODO: insert email validation code
    let email = txtEmail.value;
    let pass = txtPassword.value;
    let promise = auth.createUserWithEmailAndPassword( email, pass );
    promise.then( verifyAuth ).catch( (e) => { console.log( e.message ) } );
  } );

  btnLogout.addEventListener( "click" , e => {  // Add logout event
    auth.signOut();
  } );

  function verifyAuth(){  // Add a realtime listener to notify me of each time that fbusers auth state changes...
    return auth.onAuthStateChanged( firebaseUser => {
      // console.log( "firebaseUser " , firebaseUser );      
      if( firebaseUser ){ 
        teststring.on( 'value' , (snap) => { hotelOne.innerText = snap.val(); } );
        testobject.on( 'value' , (snap) => { console.log( snap.val() ); } );

        btnLogout.classList.remove( 'hide' );
        statusLoggedIn.classList.remove( 'hide' );

        signedOut.classList.add( 'hide' );
        signedIn.classList.remove( 'hide' );
        hotelOne.classList.remove( 'hide' );
      } 
      else 
      if ( firebaseUser === null ) { 

        btnLogout.classList.add( 'hide' );
        statusLoggedIn.classList.add( 'hide' );

        signedOut.classList.remove( 'hide' );
        signedIn.classList.add( 'hide' );
        hotelOne.classList.add( 'hide' );
        hotelOne.innerText = "";
      }
    } ); 
  }  

})();