(function() {
  
  const config = {
    apiKey: ""
    , authDomain: ""
    , databaseURL: ""
    , projectId: ""
    , storageBucket: ""
    , messagingSenderId: ""
  };
  
  let defaultApp = firebase.initializeApp(config);

  const defaultAuth = defaultApp.auth();
  const defaultStorage = defaultApp.storage();
  
  const btnLogin = document.querySelector('#btnLogin');
  const btnLogout = document.querySelector('#btnLogout');
  const statusLoggedIn = document.querySelector( '#statusLoggedIn' );
  const uploader = document.querySelector( '#uploader' );
  const fileButton = document.querySelector( '#fileButton' );
  const linkbox = document.querySelector('#linkbox');
  
  fileButton.addEventListener( "change" , changeHandler ); // listen for file selection
  function changeHandler(e) {
    e.stopPropagation();
    e.preventDefault();
    var file = e.target.files[0]; 
    var metadata = { 'contentType': file.type  };

    let storageRef = defaultStorage.ref().child( `mixed_bag/${file.name}` );
    storageRef.put( file, metadata ).then( snappy ).catch( ( err ) => {
      linkbox.classList.add( 'hide' );
      console.error('Upload failed:', err);
    } );
    
  }
  
  function snappy( snapshot ) {
    var percentage = Math.round( (snapshot.bytesTransferred / snapshot.totalBytes) * 100 );
    if(percentage < 100 ){ uploader.value = percentage; }
    else
    if(percentage === 100 ){
      uploader.value = percentage;
      var url = snapshot.downloadURL;
      linkbox.innerHTML = '<a href="' +  url + '">Click For File</a>';
      linkbox.classList.remove( 'hide' );
      console.log('Upload is complete');
    } 
  }

  btnLogin.addEventListener( "click" , e => {
    let promise = defaultAuth.signInAnonymously();
    promise.then( verifyAuth ).catch( (e) => { console.log( e.message ) } );
  } );

  btnLogout.addEventListener( "click" , e => { 
    defaultAuth.signOut();
  } );

  function verifyAuth(){ 
    return defaultAuth.onAuthStateChanged( firebaseUser => {
      if( firebaseUser ){ 
        console.log( "firebaseUser " , firebaseUser );
        btnLogin.classList.add( 'hide' );
        btnLogout.classList.remove( 'hide' );
        statusLoggedIn.classList.remove( 'hide' );
        fileButton.disabled = false;
      } 
      else 
      if ( firebaseUser === null ) { 
        btnLogin.classList.remove( 'hide' );
        btnLogout.classList.add( 'hide' );
        statusLoggedIn.classList.add( 'hide' );
        fileButton.disabled = true;
      }
    } ); 
  }

})();